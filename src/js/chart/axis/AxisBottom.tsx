import * as React from "react";

interface Props {
    height: any,
    scale: any,
}

export class AxisBottom extends React.Component<Props, {}> {
    protected elem: any;

    public componentWillReceiveProps(nextProps: any) {
        // @ts-ignore
        d3.select(this.elem).call(d3.axisBottom(nextProps.scale))
    }

    public componentDidMount() {
        // @ts-ignore
        d3.select(this.elem).call(d3.axisBottom(this.props.scale))
    }

    public render () {
        return (
            <g className={"axis-bottom"}
               transform={"translate(0," + this.props.height + ")"}
               textAnchor="middle"
               ref={elem => this.elem = elem}
            />
        )
    }
}
