import * as React from "react";
import {Switch} from "../../common/components/switch/Switch";
import {AxisBottom} from "./axis/AxisBottom";
import {AxisLeft} from "./axis/AxisLeft";
// @ts-ignore
import * as dataRaw from "./data.json";
import {Legend} from "./legend/Legend";
const data = dataRaw[0];

const colors = [
    "#2364c2",
    "#7ebd36",
    "#f7931e",
];

const margin = {top: 20, right: 20, bottom: 30, left: 50};
const width = 960 - margin.left - margin.right;
const height = 500 - margin.top - margin.bottom;

const fieldTypes = Object.keys(data[0]).filter(v => v !== "date");

export class Chart extends React.Component<{}, {}> {
    constructor(props: any) {
        super(props);
        this.state = {
            fields: fieldTypes
        }
    }

    public handleOnChange = (field: string) => (value: boolean) => {
        // @ts-ignore
        const {fields} = this.state;

        if(value) {
            this.setState({fields: [...fields, field]})
        } else {
            this.setState({fields: fields.filter((f: string) => f !== field)})
        }
    };

    public render () {
        // @ts-ignore
        const {fields} = this.state;

        // @ts-ignore
        const parseTime = d3.timeParse("%Y%m%d");
        // @ts-ignore
        const x = d3.scaleTime().range([0, width]);
        // @ts-ignore
        const y = d3.scaleLinear().range([height, 0]);
        // @ts-ignore
        const z = d3.scaleOrdinal(colors);

        // @ts-ignore
        const connectLine = d3.line()
            .x((d: any) => x(d.date))
            .y((d: any) => y(d.temperature))
        ;

        // @ts-ignore
        const displayData = fields.map((field) => ({
            id: field,
            values: data.map((item: any) => ({
                date: parseTime(item.date),
                temperature: item[field]
            }))
        }));

        // @ts-ignore
        x.domain([
            // @ts-ignore
            d3.min(displayData, (c) => d3.min(c.values, (d) => d.date )),
            // @ts-ignore
            d3.max(displayData, (c) => d3.max(c.values, (d) => d.date ))
        ]);

        y.domain([
            // @ts-ignore
            d3.min(displayData, (c) => d3.min(c.values, (d) => d.temperature )),
            // @ts-ignore
            d3.max(displayData, (c) => d3.max(c.values, (d) => d.temperature ))
        ]);

        // @ts-ignore
        z.domain(fieldTypes);

        return (
            <div style={{display: 'flex'}}>
                <svg
                    width={width + margin.left + margin.right}
                    height={height + margin.top + margin.bottom}
                >
                    <g transform={`translate(${margin.left}, ${margin.top})`}>
                        { displayData.map((item: any) => (
                            <path
                                key={item.id}
                                d={connectLine(item.values)}
                                className={"connect-line"}
                                style={{
                                    fill: 'none',
                                    strokeWidth: '1.5px',
                                    stroke: z(item.id)
                                }}
                                clipPath="url(#clip)"
                            />
                        ))}

                        <AxisBottom
                            scale={x}
                            height={height}
                        />

                        <AxisLeft
                            scale={y}
                        />
                    </g>
                </svg>

                <div>
                    {fieldTypes.map((field: any, index: number) => {
                        return (
                            <div style={{display: 'flex'}} key={index}>
                                <Legend size={15} styleProps={{background: fields.indexOf(field) > -1 ? z(field) : "#ddd"}} text={field}/>
                                <Switch value={fields.indexOf(field) > -1} onChange={this.handleOnChange(field)}/>
                            </div>
                        );
                    })}

                </div>
            </div>
        )
    }
}



